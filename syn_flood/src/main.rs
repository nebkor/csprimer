use binary_layout::prelude::*;

define_layout!(file_header, LittleEndian, {
    magic: u32,
    majver: u16,
    minver: u16,
    _tzoffset: u32,
    _tsaccuracy: u32,
    snap_len: u32,
    linktype: u32
});

define_layout!(capture_packet, LittleEndian, {
    timestamp: u32,
    musecs: u32,
    captured_len: u32,
    orig_len: u32,
});

define_layout!(ip_packet_header, BigEndian, {
    ver_ihl: u8,
    _dscp_ecn: u8,
    total_packet_len: u16,
    _bullshit: u32,
    _ttl: u8,
    prot: u8,
    _checksum: u16,
    src: u32,
    dst: u32,
});

define_layout!(tcp_header, BigEndian, {
    src: u16,
    dst: u16,
    seq_num: u32,
    ack_num: u32,
    _ignore: u8,
    flags: u8
});

// capture packet header
const CPH_LEN: usize = 16;

fn main() {
    let capture = std::fs::read("synflood.pcap").unwrap();
    let flen = capture.len();

    let fh = file_header::View::new(&capture[0..24]); // file header is 24 bytes
    let magic = fh.magic().read();
    let majver = fh.majver().read();
    let minver = fh.minver().read();
    let snap_len = fh.snap_len().read();
    let linktype = fh.linktype().read();

    println!("{magic:#x}, {majver:#x}, {minver:#x}, {snap_len}, {linktype:#x}");

    let mut cursor = 24; // start position of first packet header
    let mut packet_count = 0;
    let mut syns = 0;
    let mut acks = 0;
    while cursor < flen {
        let pend = cursor + CPH_LEN;
        let pview = capture_packet::View::new(&capture[cursor..pend]);
        let _ts = pview.timestamp().read();
        let caplen = pview.captured_len().read() as usize;

        let ipstart = pend + 4;

        let ipp = ip_packet_header::View::new(&capture[ipstart..(ipstart + 20)]);
        let ihl = ipp.ver_ihl().read() & 0x0f;
        assert_eq!(5, ihl);

        let tcp_start = ipstart + (4 * ihl as usize);
        let tcp_end = tcp_start + 14; // just the first 14 bytes of the tcp header
        let tcpp = tcp_header::View::new(&capture[tcp_start..tcp_end]);

        let src = tcpp.src().read();
        let dst = tcpp.dst().read();
        let flags = tcpp.flags().read();
        let is_syn = (flags & 0x02) != 0 && dst == 80;
        let is_ack = (flags & 0x10) != 0 && src == 80;

        if is_syn {
            syns += 1;
        }
        if is_ack {
            acks += 1;
        }

        cursor += caplen + CPH_LEN;
        packet_count += 1;
    }
    dbg!(packet_count, syns, acks, acks as f32 / syns as f32);
}

fn from4le(bytes: &[u8]) -> u32 {
    u32::from_le_bytes(bytes.try_into().unwrap())
}

fn from2le(bytes: &[u8]) -> u16 {
    u16::from_le_bytes(bytes.try_into().unwrap())
}

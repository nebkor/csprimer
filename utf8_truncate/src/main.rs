use std::fs;

fn main() {
    let cases = fs::read("cases").unwrap();
    let mut newlines = Vec::new();
    for (i, b) in cases.iter().enumerate() {
        if *b == 0x0a_u8 {
            newlines.push(i);
        }
    }

    let mut lines = Vec::new();
    let mut prev = 0usize;
    for &newline in newlines.iter() {
        let line = cases[prev..=newline].to_vec();
        lines.push(line);
        prev = newline + 1;
    }

    let mut results = String::new();
    for line in lines.iter() {
        let trunc = line[0] as usize;
        let line = &line[1..];
        let len = line.len();
        let mut trunc = trunc.min(len - 1);
        while trunc > 0 {
            let bound = &line[trunc];
            if (0b10000000..=0b10111111).contains(bound) {
                trunc -= 1;
            } else {
                break;
            }
        }
        // trunc falls on a valid start character so it can be excluded
        let bytes = &line[0..trunc];
        let mut s = String::from_utf8(bytes.to_vec()).unwrap();
        if !s.ends_with('\n') {
            s.push('\n');
        }
        results += &s;
    }

    fs::write("results", results).unwrap();
}

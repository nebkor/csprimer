fn main() {
    use header::*;

    let bytes: Vec<u8> = std::fs::read("test-data/stretch-goal.bmp").unwrap();

    let file_size = from4le(&bytes[FILE_SIZE]);
    let offset = from4le(&bytes[IMG_DATA_OFFSET]);
    let header_size = from4le(&bytes[HEADER_SIZE]);
    let width = from4le(&bytes[IMG_WIDTH]);
    let height = from4le(&bytes[IMG_HEIGHT]);
    let color_planes = from2le(&bytes[COLOR_PLANES]);
    let pixel_depth = from2le(&bytes[PIXEL_DEPTH]);
    let compression = from4le(&bytes[COMPRESSION]);
    let img_size = from4le(&bytes[IMG_SIZE]);
    let hor_res = from4le(&bytes[HRES]);
    let vert_res = from4le(&bytes[VRES]);

    dbg!(
        file_size,
        offset,
        header_size,
        width,
        height,
        color_planes,
        pixel_depth,
        compression,
        img_size,
        hor_res,
        vert_res
    );

    let mut img = vec![0u8; file_size as usize];
    let img = img.as_mut_slice();
    let row_len = (width * pixel_depth as u32) as usize;
    let row_size = ((row_len + 31) / 32) * 4;
    let new_row_size = (((height * pixel_depth as u32 + 31) / 32) * 4) as usize;
    let bpp = pixel_depth as usize / 8; // bytes per pixel
    for row in (0..height as usize).rev() {
        //
        for col in 0..width as usize {
            let addr = ((height as usize - row - 1) * row_size) + col * bpp + offset as usize;

            let new_col = height as usize - row - 1;
            let new_row = col;
            let new_addr =
                ((width as usize - new_row - 1) * new_row_size) + new_col * bpp + offset as usize;
            //println!("{row}, {col} --> {new_row}, {new_col}");
            //println!("{addr} -> {new_addr}");
            img[new_addr..(new_addr + bpp)].copy_from_slice(&bytes[addr..(addr + bpp)]);
        }
    }
    img[0..header_size as usize].copy_from_slice(&bytes[0..header_size as usize]);

    img[IMG_HEIGHT].copy_from_slice(&width.to_le_bytes());
    img[IMG_WIDTH].copy_from_slice(&height.to_le_bytes());

    std::fs::write("test-data/stretch.bmp", img).unwrap();
}

fn from4le(bytes: &[u8]) -> u32 {
    u32::from_le_bytes(bytes.try_into().unwrap())
}

fn from2le(bytes: &[u8]) -> u16 {
    u16::from_le_bytes(bytes.try_into().unwrap())
}

mod header {
    use std::ops::Range;

    pub const FILE_SIZE: Range<usize> = 0x02..0x06;
    pub const IMG_DATA_OFFSET: Range<usize> = 0x0a..0x0e;
    pub const HEADER_SIZE: Range<usize> = 0x0e..0x12;
    pub const IMG_WIDTH: Range<usize> = 0x12..0x16;
    pub const IMG_HEIGHT: Range<usize> = 0x16..0x1a;
    pub const COLOR_PLANES: Range<usize> = 0x1a..0x1c;
    pub const PIXEL_DEPTH: Range<usize> = 0x1c..0x1e;
    pub const COMPRESSION: Range<usize> = 0x1e..0x22;
    pub const IMG_SIZE: Range<usize> = 0x22..0x26;
    pub const HRES: Range<usize> = 0x26..0x2a;
    pub const VRES: Range<usize> = 0x2a..0x2e;
}

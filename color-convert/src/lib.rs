use lazy_regex::{regex, Captures};

pub fn parse_line(line: &str) -> String {
    let cre = regex!(r"(color:\s*)(#[[:xdigit:]]{3,8})");

    cre.replace_all(line, |caps: &Captures| {
        format!("{} {}", &caps[1].trim(), hex2dec(&caps[2]))
    })
    .to_string()
}

fn hex2dec(color: &str) -> String {
    let color = color.trim_start_matches('#');

    match color.len() {
        3 | 4 => short(color),
        6 | 8 => long(color),
        _ => format!("#{color}"),
    }
}

fn long(color: &str) -> String {
    let mut bytes = Vec::new();
    let alpha = color.len() == 8;
    for i in (0..color.len()).step_by(2) {
        let s = &color[i..i + 2];
        let v = u8::from_str_radix(s, 16).unwrap();
        if alpha && bytes.len() == 3 {
            bytes.push("/".to_string());
            let v = (v as f32) / 255.0;
            let v = format!("{:.5}", v);
            bytes.push(v);
        } else {
            bytes.push(v.to_string());
        }
    }
    let rgb = if alpha { "rgba" } else { "rgb" };
    let vals = bytes.join(" ");
    format!("{rgb}({vals})")
}

fn short(color: &str) -> String {
    let color: String = color.chars().map(|c| format!("{c}{c}")).collect();
    long(&color)
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn it_works() {}
}

use std::io::{self as stdio, Write};

fn main() -> stdio::Result<()> {
    let stdin = stdio::read_to_string(stdio::stdin())?;

    for line in stdin.lines() {
        stdio::stdout().write_all(color_convert::parse_line(line).as_bytes())?;
        stdio::stdout().write_all(&[0x0Au8])?;
        stdio::stdout().flush()?;
    }

    Ok(())
}

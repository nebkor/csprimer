#[derive(Clone, Copy, Debug, PartialEq, Eq, PartialOrd, Ord)]
pub struct VarInt {
    val: u64,
}

pub type Bytes = Vec<u8>;

const MASK: u8 = 127;
const CONT: u8 = 128;

impl VarInt {
    pub fn encode(&self) -> Bytes {
        let mut val = self.val.to_le();
        let val = &mut val;
        let mut bytes = Vec::with_capacity(10);
        while val > &mut 0 {
            let mut byte = (*val & 127) as u8;
            *val >>= 7;
            if *val > 0 {
                byte |= CONT;
            }
            bytes.push(byte);
        }
        if bytes.is_empty() {
            bytes.push(0);
        }

        bytes
    }

    pub fn decode(bytes: &[u8]) -> Self {
        let mut val: u64 = 0;
        let val = &mut val;
        for byte in bytes.iter().rev() {
            *val <<= 7;
            let byte = byte & MASK;
            *val |= byte as u64;
        }

        Self { val: val.to_le() }
    }

    pub fn value(&self) -> u64 {
        self.val
    }
}

impl PartialEq<u64> for VarInt {
    fn eq(&self, other: &u64) -> bool {
        self.val == *other
    }
}

impl PartialEq<VarInt> for u64 {
    fn eq(&self, other: &VarInt) -> bool {
        *self == other.value()
    }
}

impl From<u64> for VarInt {
    fn from(value: u64) -> Self {
        Self { val: value }
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn decode_150() {
        let bytes: [u8; 2] = [0b10010110u8, 0b00000001u8]; // from https://protobuf.dev/programming-guides/encoding/#varints
        let out = VarInt::decode(&bytes);
        dbg!(out);
        assert_eq!(out, 150);
    }

    #[test]
    fn decode_max() {
        let bytes = [0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 1]; // the last byte there could be any value > 0
        let out = VarInt::decode(&bytes);
        dbg!(out);
        assert_eq!(out, u64::MAX.to_le());
    }

    #[test]
    fn encode_150() {
        let val: VarInt = 150.into();
        let bytes = val.encode();
        let decoded = VarInt::decode(&bytes);
        assert_eq!(val, decoded);

        assert_eq!(bytes, vec![0x96, 1]);
    }

    #[test]
    fn encode_max() {
        let bytes = [0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 1];
        let val: VarInt = (u64::MAX).to_le().into();
        let encoded = val.encode();

        dbg!(&encoded);
        assert_eq!(&bytes, &encoded.as_slice());
    }

    #[test]
    fn one_and_zero() {
        let one: VarInt = 1.into();
        let zero: VarInt = 0.into();
        assert_eq!(vec![0], zero.encode());
        assert_eq!(vec![1], one.encode());
    }

    #[test]
    fn round_trips() {
        // test a billion round trips
        for i in 0..500_000_000u64 {
            let even: VarInt = (i * 4).to_le().into();
            let odd: VarInt = (i * 7).to_le().into();

            assert_eq!(even, VarInt::decode(&even.encode()));
            assert_eq!(odd, VarInt::decode(&odd.encode()));
        }
    }
}

fn main() {
    let num = 324_485_007u64;
    for (i, b) in num.to_be_bytes().iter().enumerate() {
        println!("BE: {i}: {b}");
    }

    println!();

    for (i, b) in num.to_le_bytes().iter().enumerate() {
        println!("LE: {i}: {b}");
    }

    println!("now just one:");

    let mut one = 1u64;
    for (i, b) in one.to_le_bytes().iter().enumerate() {
        println!("{i}: {b}");
    }
    one <<= 7;
    println!("shift left seven: {one}");
    for (i, b) in one.to_le_bytes().iter().enumerate() {
        println!("{i}: {b}");
    }
    one |= 127;
    println!("or-eq 127: {one}");
    for (i, b) in one.to_le_bytes().iter().enumerate() {
        println!("{i}: {b}");
    }
}

use std::io::{self as stdio, Write};

fn main() -> stdio::Result<()> {
    let stdin = stdio::read_to_string(stdio::stdin())?;

    for line in stdin.lines() {
        let beeps = (0..line.len()).map(|_| 0x07u8).collect::<Vec<_>>();
        stdio::stdout().write_all(&beeps)?;
        stdio::stdout().flush()?;
    }

    eprint!("\0x07");

    Ok(())
}

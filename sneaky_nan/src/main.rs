fn main() {
    let mut x = conceal("hello");
    assert!(x.is_nan());

    x += 1.0;

    assert_eq!("hello".to_string(), extract(x));

    assert_eq!("123456".to_string(), extract(conceal("1234567890")));
    assert_eq!("123".to_string(), extract(conceal("123")));
}

fn conceal(msg: &str) -> f64 {
    let len = msg.len().min(6); // max len is 6
    let mut bytes = [0u8; 8];
    let msg = msg.as_bytes();

    bytes[0..len].copy_from_slice(&msg[0..len]);
    bytes[6] = 0xfc;
    bytes[7] = 0x7f;
    f64::from_le_bytes(bytes)
}

fn extract(x: f64) -> String {
    let bytes = x.to_le_bytes();
    let bytes = &bytes[0..6];
    let s = String::from_utf8(bytes.to_vec()).unwrap();
    s.trim_end_matches('\0').to_string()
}
